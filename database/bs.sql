

CREATE TABLE IF NOT EXISTS tasks(
     id SERIAL PRIMARY KEY,
     title varchar(100) NOT NULL ,
     description varchar(250) ,
     created_on timestamp with time zone DEFAULT current_timestamp

);


INSERT INTO  tasks(title,description ) values( 'Tasks 1 ' , 'Description 1 ');