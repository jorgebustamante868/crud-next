import { Button, ButtonContent, Card, CardContent, Form, Grid, GridColumn, Icon , Confirm } from "semantic-ui-react";
import {ChangeEvent, FormEvent, useState , useEffect} from 'react'
import { Task } from "src/interface/Task";

import { useRouter } from "next/router";
import Layout from "src/componentes/Layout";
export default function newPage() {

    const [task, setTask] = useState ({
        title:"",
        description:""
    })

    const [openConfirm , setOpenConfirm] = useState(false)
    // const handleChange = (e : ChangeEvent<HTMLInputElement>) => {
    //     console.log(e.target.name , e.target.value)
    // }
    const router = useRouter();

    const handleChange =({ 
        target:{name , value },
    }: ChangeEvent<HTMLInputElement|HTMLTextAreaElement>) => setTask({ ...task, [name]:value})





    const createTask = async (task:Task) => {
        await fetch('http://localhost:3001/api/tasks' , {
            method:'POST',
            headers:{
                'Content-Type': 'application/json'
            },
            body:JSON.stringify(task)
        })
    }




    const updateTask = async ( id:string , task:Task ) => {
        await fetch('http://localhost:3001/api/tasks/'+id , {
            method:'PUT',
            headers:{
                'Content-Type': 'application/json'
            },
            body:JSON.stringify(task),
        })
    }

    // Para verificar si se mando un id para editar  o eliminar

    const loadTask = async ( id : string) => {
        const res = await fetch('http://localhost:3001/api/tasks/'+id)
        const task = await res.json()
        const title = task[0].title;
        const description = task[0].description;
        setTask( { title:title , description:description  } )
        console.log("Obteniendo informacion " + JSON.stringify(task) +" Si obtiene " );
        console.log("obteniendo titulo"+ title)
    }



    const handleSubmit = async (e:FormEvent<HTMLFormElement>) => {
        e.preventDefault()
        
        try {
            if ( typeof router.query.id === "string" ) 
                updateTask(router.query.id , task)
            else{
                
                await createTask(task);
            }
            
            router.push('/')

        } catch (error) {
            console.log(error)
        }

        // console.log(tasks)
    }

    const handleDelete = async (id:string) => {

    try {
        const res = await fetch('http://localhost:3001/api/tasks/'+id , {
            method:"DELETE"
        })
        router.push('/')        
    } catch (error) {
        console.log(error)
    }

    }

    useEffect ( () =>{

        if( typeof router.query.id === "string" ) loadTask(router.query.id)

    }, [ router.query])



    return (
    <Layout>
        <Grid centered columns={3} verticalAlign="middle" style={{ height : '70%'}}>
            <GridColumn>

            <Card>
                <CardContent>
                    <form onSubmit={handleSubmit}>
                        <Form.Field>
                        <label htmlFor='title' >Title de la Tarea</label>
                        <input 
                            type="text" 
                            placeholder="Escribe un title" 
                            name = 'title' 
                            onChange={handleChange}
                            value= {task.title}
                            ></input>
                        </Form.Field>
                        <Form.Field>
                        <label htmlFor='title' >Description de la Tarea</label>
                        <textarea 
                            placeholder="Escribe tu  description"
                            name = 'description' 
                            rows={3} 
                            onChange={handleChange}
                            value= {task.description}    
                        ></textarea>
                        </Form.Field>
                        {
                            router.query.id ? (

                                <Button color ="teal">
                                    <Icon name="save"></Icon>
                                    Actualizar
                                </Button>

                            ):(

                                <Button primary>
                                    <Icon name="save"></Icon>
                                    Guardar
                                </Button>  
                            )
                        }

                    </form>

                </CardContent>
            </Card>
            { 
            router.query.id && (
                <Button  
                color="red"
                onClick={ ()=> setOpenConfirm(true)}
                
            >
                <Icon name="delete"></Icon>
                        Delete
            </Button>

            )
           }

        
            </GridColumn>
        </Grid>
        <Confirm
        header='Delete a task'
        content="Seguro de eliminar la Task"
        open={ openConfirm }

        onCancel={ ()=> setOpenConfirm(false) }
        onConfirm={ () =>typeof router.query.id==="string" && handleDelete(router.query.id)}
        >

        </Confirm>
    </Layout>    
  )
}
