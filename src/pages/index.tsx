
import React from 'react'
import { Button, Grid, GridColumn, GridRow } from 'semantic-ui-react'

import {Task} from 'src/interface/Task' 

import { useRouter } from 'next/router'
import TaskList from 'src/componentes/tasks/TaskList'
import Layout from 'src/componentes/Layout'

interface Props {
  tasks: Task[]
}

export default function index( {tasks}:Props ) {
  
  const router = useRouter();

  return (
    <Layout>
    { 
      tasks.length === 0 ? (
  
          <Grid columns={3} centered verticalAlign='middle' style={{ height:'70%'}}>
            <GridRow>
              <GridColumn>
   
                <Button onClick={ () => router.push('tasks/new') }> Agregar Tarea</Button>
              </GridColumn>
            </GridRow>

          </Grid>
        ) : (
          <Grid columns={3} centered verticalAlign='middle' style={{ height:'70%'}}>
            <TaskList tasks={ tasks }></TaskList>
          </Grid>
          )
    }
    </Layout>
  )
}

export const getServerSideProps = async () => {

  const res = await fetch('http://localhost:3001/api/tasks')
  const tasks = await res.json()
  console.log(tasks)
  
  return {
    props:{
      tasks
    }
  }

}