
import {NextApiRequest , NextApiResponse} from 'next'
import { conn } from 'src/utils/database';

export default async ( req : NextApiRequest , res:NextApiResponse ) => {

  const { method , query , body } = req;

  switch (method){

    case 'GET':

      try {
        const consult= 'SELECT * FROM tasks WHERE id = $1'
        const value = [query.id]
        console.log(value)
        const response = await conn.query(consult , value)
        
        if ( response.rows.length === 0 )
          return res.status(404).json({ message: "Registro no encontrado "})
        
        return res.status(200).json(response.rows)

    } catch (error:any) {
        return res.status(500).json({ message: error.message })
    }

    
   
    case 'PUT':

      try {
        const consult= 'UPDATE tasks set title = $1 , description = $2 where id = $3 RETURNING *'
        const {title , description } = body
        const value = [title , description, query.id]
        console.log(value)
        const response = await conn.query(consult , value)
        
        if ( response.rowCount === 0 )
          return res.status(404).json({ message: "Registro no encontrado "})
        
        return res.status(200).json(response.rows)

    } catch (error:any) {
        return res.status(500).json({ message: error.message })
    }

    case 'DELETE':

      try {
        const consult= 'DELETE FROM tasks WHERE id = $1 RETURNING *'
        const value = [query.id]
        const response = await conn.query(consult , value)
        
        console.log(value)
        if ( response.rowCount === 0 )
          return res.status(404).json({ message: "Task no encontrado "})
                
        return res.status(200).json(response.rows[0])

    } catch (error:any) {
        return res.status(500).json({ message: error.message })
    }

    default:
    return res.status(400).json('invalid metodo')
  }

}
