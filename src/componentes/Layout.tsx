
import React from 'react'
import { Container } from 'semantic-ui-react'
import Narvar from './Narvar'

export default function Layout( {children }: { children : JSX.Element}) {
  return (
    <div>
        <Narvar></Narvar>

        <main style = {{ background: '#212121' }}>
        <Container style={{ paddingTop : '2rem' , height : '90vh'}}>
        { children}

        </Container>
        </main>
    </div>
  )
}
