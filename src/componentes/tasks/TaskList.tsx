import React from 'react'
import { Card, CardContent, CardDescription, CardGroup, CardHeader, CardMeta } from 'semantic-ui-react';
import { Task } from 'src/interface/Task'

import {useRouter } from 'next/router'

interface Props {
    tasks:Task[];
}


export default function TaskList( {tasks }:Props ) {

    const router = useRouter()
    console.log(tasks)
  return (
    <CardGroup itemsPerRow={ 4 }>
        {tasks.map(task =>(
            <Card key={ task.id} onClick= { () => router.push(`/tasks/edit/${task.id}` ) }>
                <CardContent>
                    <CardHeader> { task.title} </CardHeader>
                    <CardDescription> { task.description } </CardDescription>
                    { task.created_on && (
                        
                        <CardMeta> { new Date( task.created_on ).toLocaleDateString()}</CardMeta>
                    ) }
                </CardContent>
            </Card>

        ) ) }
    </CardGroup>
  )
}
