
import React from 'react'
import { Button, Container, Image, Menu, MenuItem, MenuMenu } from 'semantic-ui-react'

import { useRouter } from 'next/router'


export default function Narvar() {

    const router = useRouter()
    
    return (
    <Menu inverted attached style= {{ padding:'1.3rem' }}>
        <Container>
            <MenuItem>
                <Image 
                src='https://react.semantic-ui.com/logo.png'
                width={30}
                height= {30}
                alt='logo'
                href='http://localhost:3001'
                ></Image>
            </MenuItem>

            <MenuMenu position='right'>
                <MenuItem>
                    <Button onClick={ () => router.push('/tasks/new') }>
                        new Task
                    </Button>
                </MenuItem>
            </MenuMenu>
        </Container>
    </Menu>
  )
}
